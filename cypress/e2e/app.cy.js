describe('JSONPlaceholder API Tests', () => {
  it('Perform a GET request to search by name attribute', () => {
    cy.request('GET', 'https://jsonplaceholder.typicode.com/comments', { qs: { name: 'alias odio sit' } })
      .then((response) => {
        expect(response.status).to.equal(200);
        expect(response.body).to.not.be.empty;
        expect(response.body[0].email).to.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
      });
  });

  it('Perform a POST request to create a user', () => {
    const user = {
      name: 'John',
      username: 'john',
      email: 'john@gmail.com'
    };

    cy.request({
      method: 'POST',
      url: 'https://jsonplaceholder.typicode.com/users',
      body: user
    }).then((response) => {
      expect(response.status).to.equal(201);
      expect(response.body).to.have.property('id').to.be.a('number');
    });
  });

  it('Perform a PUT request to update a user', () => {
    const userId = 5;

    const updatedUserData = {
      email: 'newemail@gmail.com',
      lat: 40.7128,
      lng: -74.0060 
    };

    cy.request({
      method: 'PUT',
      url: `https://jsonplaceholder.typicode.com/users/${userId}`,
      body: updatedUserData,
    }).then((response) => {
      expect(response.status).to.equal(200);
      expect(response.body).to.deep.equal({
        ...updatedUserData,
        id: userId
      });
    });
  });
});