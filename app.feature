Feature: JSONPlaceholder API Tests

  Scenario: Search for comments by name attribute
    Given I make a GET request to the endpoint "https://jsonplaceholder.typicode.com/comments" with the query parameter "name" set to "alias odio sit"
    Then the response status code should be 200
    And the response body should not be empty
    And the email of the first comment should match the email pattern

  Scenario: Create a new user
    Given a user with name "John", username "john", and email "john@gmail.com"
    When I make a POST request to the endpoint "https://jsonplaceholder.typicode.com/users" with the user data
    Then the response status code should be 201
    And the response body should contain an ID

  Scenario: Update user information
    Given the user ID is 5
    And new email, latitude, and longitude values are provided
    When I make a PUT request to the endpoint "https://jsonplaceholder.typicode.com/users/5" with the updated user data
    Then the response status code should be 200
    And the response body should match the updated user data